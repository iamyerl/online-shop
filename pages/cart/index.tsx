import React, {useEffect, useState} from 'react';
import {ICart} from "../../types/product";
import MainLayout from "../../layouts/MainLayout";
import c from "../../styles/cartPage.module.css"
const Index = () => {
    const [product, setProduct] = useState<ICart[]>([])
    const [subTotal, setSubtotal] = useState(0)
    let sum = 0;
    useEffect(() => {
        const cartInfo = JSON.parse(localStorage.getItem('cartInfo') || '[]') as ICart[]
        console.log(cartInfo)
        setProduct(cartInfo)
        for (let i = 0; i < cartInfo.map(v=> v.count*v.product.price).length; i++) {
            sum += cartInfo.map(v=> v.count*v.product.price)[i];
        }
        setSubtotal(sum);
    }, [])

    return (
        <MainLayout>
            <div className={c.container}>
                <div className={c.prods}>
                    {product.map( (p,index) =>
                        <div className={c.prodCard} key={`cartInfo-${index}`}>
                            <div className={c.img}>IMG</div>
                            <div className={c.prodInfo}>
                                <div>
                                    <h3>{p.product.name}</h3>
                                    <p>Quantity : {p.count}</p>
                                </div>
                                <h3>Total price: ${p.count * p.product.price}</h3>
                            </div>
                        </div>
                    )}
                </div>
                <div className={c.order}>
                    <div className={c.orderCard}>
                        <h2>Order Summary </h2>
                        <div className={c.df}>
                            <p>Shipping</p>
                            <p>Free</p>
                        </div>
                        <div className={c.df}>
                            <p>Sales Tax</p>
                            <p>10$</p>
                        </div>

                        <div className={c.df}>
                            <h3>Subtotal</h3>
                            <h3>$ {subTotal}</h3>
                        </div>

                        <button className={c.checkout}>
                            CHECKOUT
                        </button>
                    </div>
                </div>
            </div>
        </MainLayout>
    );
};

export default Index;
