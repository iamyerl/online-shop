import React from 'react';
import {IProduct} from "../types/product";
import ProductItem from "./ProductItem";
import pL from "../styles/productList.module.css"
interface ProductListProps {
    products: IProduct[]
}
const ProductList: React.FC<ProductListProps> = ({products}) => {
    return (
        <div className={pL.prodList} >
            {products.map((product,idx) =>
               <React.Fragment key={idx}>
                   <ProductItem product={ product }/>
               </React.Fragment>
            )}
        </div>
    );
};

export default ProductList;
