import React from 'react';
import {IProduct} from "../types/product";
import {useRouter} from "next/router";
import pI from "../styles/productItem.module.css"

interface ProductItemProps {
    product: IProduct,
}
const ProductItem: React.FC<ProductItemProps> = ({product}) => {
    const router = useRouter()
    return (
        <div className={pI.card} onClick={()=> router.push('/products/'+product.id)}>
            <div className={pI.img}>IMG</div>
            <p className={pI.name}>{ product.name }</p>
            <p className={pI.price}>${ product.price }</p>
            <button onClick={()=> router.push('/products/'+product.id)}>Buy</button>
        </div>
    );
};

export default ProductItem;
