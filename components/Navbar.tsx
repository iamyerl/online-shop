import n from "../styles/navbar.module.css";
import Link from "next/link";

export default function Navbar() {
    return (
        <>
            <ul className={n.list}>
                <li>
                    <Link href={"/"}>Shop</Link>
                </li>
                <li>
                    <Link href={"/products"}>Product</Link>
                </li>
                <li>
                    <Link href={"/cart"}>Cart</Link>
                </li>
            </ul>
        </>
    )
}
