export interface IProduct {
    id: number,
    name: string,
    price: number
}

export interface ICart {
    product: IProduct;
    count: number;
}